# refactor-service-rappi

Repositorio dedicado a solucionar la segunda prueba de Rappi [FE].

### Requerimiento

Refactorizar la clase AddressService que está dentro de AddressService.ts
No es necesario hacer funcionar el código
  
### Aspectos a revisar

  - Sintaxis
  - Variables innecesarias
  - Condiciones logicas
  - Codigo duplicado
  - Encapsulamiento
  - Factorizacion
  - Logica de desarrollo
  - Coherencia
  - Legibilidad

### Descripción General

Para refactorizar código es necesario saber el objetivo de la función y la lógica de desarrollo usada. Tome en cuenta **Aspectos a revisar** para mejorar legibilidad, evitar lógica de desarrollo innecesaria y por lo tanto alcanzar una huella de desarrollo menor. 

El servicio permitió mejorar las asignaciones condicionadas, puntos de retorno confusos, encapsulamiento/modularización de segmentos lógicos, factorización condiciones innecesarias y código duplicado. Tambien se renombraron algunas variables y se eliminaron otras ya que solo se usaban una vez a fin de mejorar la legibilidad. Finalmente se reorganizó la declaración de variables en orden alfabetico.

### Notas
- Mayor detalle sobre los cambios realizados y las razones estan descritos en los mensajes de los commits.
- Las funciones de google estaban siendo utilizadas desde el objeto windows, el cual por defecto no tiene estas funciones, aun cuando se pudo haber extendido, asumi que era parte de la prueba arreglarlo.
- El localStorage estuvo siendo utilizado como si pudiera manejar objectos sin embargo la documentacion indica que solo maneja strings, por lo que asumi que era parte de la prueba arreglarlo.
- En algunas ocasiones se usaban variables como key del localStorage al almacenar contenido. Aunque es posible que sea parte de la prueba cambiar esas variables a strings estaticos, asumi que la logica de negocio requiere keys dinamicos que estan almacenados en aquellas variables.