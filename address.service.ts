export class AddressService {
  
  public $;
  public addressTextBoxValue;
  public cart;
  public currentAddress;
  public currentlyLoading;
  public description;
  public flashMessages;
  public flow;
  public latCurrentAddress;
  public lngCurrentAddress;
  public ENV;
  public params;
  public removeAddressList;
  public resultLogin;  
  public selectedCountry;
  public selectedTag;
  public serverUrl;
  public showAddAddressToUpdate;
  public showModal;
  public showPopUp;
  public showReloadWarning;
  public store;
  public storeType;
  public targetObject;


  addressChangeAction(stLat, stLng, stAddress, stAddressId, stStoreType) {
    let result = this.resultLogin;
    this.updateLocalStorageAddress(stLat, stLng, stAddressId, stAddressId, result);

    if (this.flow === 'login') {
      localStorage.setItem("id", result.id);

      if (!this.params) {
        this.showAddAddressToUpdate = false;
        this.showModal = (window.location.toString().split("#")[1] === undefined);
      }
    } else {
      let storeType = localStorage.getItem(stStoreType);
      let shippingAddress = (({id, address, description, lng, lat, tag, active, lastorder}) => ({ id, address, description, lng, lat, tag, active, lastorder}))(result);

      this.cart.setShippingAddress(storeType, shippingAddress);
      this.showPopUp = this.currentlyLoading = this.removeAddressList = false;      

      if (this.flow === 'change-direction') {
        window.location.reload(true);
      }
    }
  }


  submit(stContent, stLat, stLng, stAddressId, stAddress, stStoreType) {
    var param = {
      stContent: stContent,
      stLat: stLat,
      stLng: stLng,
      stAddressId: stAddressId,
      stAddress: stStoreType
    };

    if (!this.selectedTag) {
      this.selectedTag = 'otra';
    }

    if (this._isAddressValid()) {
      this.currentlyLoading = true;
      let geocoder = new google.maps.Geocoder();
      let currentLocation = {lat: parseFloat(this.latCurrentAddress), lng: parseFloat(this.lngCurrentAddress)};

      geocoder.geocode({'location': currentLocation}, this._processLocation.bind(this, param));
    }
  }

  private createAddress(param, result) {
      let newAddress = this.store.createRecord('address', {
        address: this.currentAddress,
        description: this.description,
        lat: this.latCurrentAddress,
        lng: this.lngCurrentAddress,
        tag: this.selectedTag,
        active: true
      });
      
      newAddress.save()
        .then(this.processFlowState.bind(this, param));
  }

  private getCurrentCountry(stContent){
    return (localStorage.getItem(stContent) && JSON.parse(localStorage.getItem(stContent)).countryName) || this.ENV.location[0].name;
  }
  
  private isAddressValid(){
    var result = true;
    
    if (!this.addressTextBoxValue || !this.currentAddress) {
      this.flashMessages.info('La dirección no puede dejarse en blanco');
      result = false;
    } else if (!this.latCurrentAddress  && !this.lngCurrentAddress) {
      this.flashMessages.info('Por favor, seleccione una dirección válida');
      result = false;
    } 

    return result;
  }
  
  private processFlowState(param, result) {
    if (this.flow === 'checkout') {
      let storeType = localStorage.getItem(param.stStoreType);
      let shippingAddress = (({id, address, description, lng, lat, tag, active, lastorder}) => ({ id, address, description, lng, lat, tag, active, lastorder}))(result);
      this.updateLocalStorageAddress(param.stLat, param.stLng, param.stAddressId, param.stAddressId, shippingAddress);
      this.cart.setShippingAddress(storeType, shippingAddress);
      this.currentlyLoading = false;
    } else if (this.flow === 'profile') {
      this.showPopUp = false;
    } else if (this.flow === 'login' || this.flow === 'change-direction') {
      this.resultLogin = result;
      let newLat = Math.abs(Number(result.lat)) || 0;
      let newLng = Math.abs(Number(result.lng)) || 0;
      let currentLat = Math.abs(parseInt(localStorage.getItem(param.stLat))) || 0;
      let currentLng = Math.abs(parseInt(localStorage.getItem(param.stLng))) || 0;
      let cart = this.cart.getCart(localStorage.getItem(param.stStoreType));
      this.showReloadWarning = ((Math.abs(newLat - currentLat) > 0.000001 || Math.abs(newLng - currentLng) > 0.000001) && cart && cart.cartItems.length);
    }
  }

  private processLocation(param, results) {
      let currentCountry = this._getCurrentCountry(param.stContent);
      let component = results[0].address_components.filter(component => component.types[0] === 'country');
      this.selectedCountry = component.long_name;

      if ((currentCountry === this.selectedCountry || this.selectedCountry === 'Mexico') && this.latCurrentAddress && this.lngCurrentAddress) {
        let lat = this.latCurrentAddress;
        let lng = this.lngCurrentAddress;
        
        this._searchStore(lat, lng)
          .then(this.createAddress.bind(this, param))
          .fail(this.showErrorNotifications);
      } else {
        this.flashMessages.danger('Por favor, añadir una dirección de ' + currentCountry);
        this.currentlyLoading = false;
      }
  }

  private searchStore(lat, lng) {
    return this.$.ajax({
      type: "GET",
      url: `${this.serverUrl}${this.ENV.searchStore}lat=${lat}&lng=${lng}`,
    });
  }

  private showErrorNotifications(err) {
    this.currentlyLoading = false;
    let errMsg = `${err.statusText}: `;
    if (err.responseJSON && err.responseJSON.error) {
      errMsg = err.responseJSON.error.message;
    }
    this.flashMessages.danger(errMsg);
  }

  private updateLocalStorageAddress(stLat, stLng, stAddress, stAddressId, result) {
    localStorage.setItem(stLat, result.lat);
    localStorage.setItem(stLng, result.lng);
    localStorage.setItem(stAddressId, result.id);
    localStorage.setItem(stAddress, result.address);
  }
}
